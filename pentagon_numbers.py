import time
start_time = time.time()

def find_pair_pentagonal_numbers(limit):
    pentagonal_numbers = [int(x*(3*x-1)*0.5) for x in list(range(1,limit))]
    last_min = 50
    for i in range(0,len(pentagonal_numbers)):
        for j in range(i,len(pentagonal_numbers)):
            if (int(abs(pentagonal_numbers[j] - pentagonal_numbers[i])) in pentagonal_numbers) \
                    and ((pentagonal_numbers[i] + pentagonal_numbers[j]) in pentagonal_numbers):
                print("work ! ")
                print(str(pentagonal_numbers[i]) + '  ' + str(pentagonal_numbers[j]))
                last_min = (pentagonal_numbers[i] - pentagonal_numbers[j])
    return last_min

print(find_pair_pentagonal_numbers(3000))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )